#include <iostream>
#include <fstream>
#include <filesystem>
#include <cassert>

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "fcexplorer.h"

/* Manual test:
 * xmllint --noout --schema ../../fcdocument.xsd ./Document.xml
 */

namespace
{
  bool fuzzyEqual(double d0, double d1)
  {
    return std::fabs(d0 - d1) < std::numeric_limits<float>::epsilon();
  }
  
  struct TestVisitor
  {
    void visitObject(const Object &o)
    {
      std::cout << "  object: " << std::endl
      << "    name: " << o.name();
      if (o.type().present())
        std::cout << std::endl << "    type: " << o.type().get();
      if (o.id().present())
        std::cout << std::endl << "    id: " << o.id().get();
      std::cout << std::endl;
    }
    
    void visitObjectData(const Object &o)
    {
      std::cout << "  object data: " << std::endl
      << "    name: " << o.name() << std::endl;
    }
    
    void visitObjectDataProperty(const Property &p)
    {
      std::cout << "    Property: " << std::endl
      << "      name: " << p.name() << std::endl
      << "      type: " << p.type() << std::endl;
      if (p.Integer().present()) std::cout << "      Integer: " << p.Integer()->value() << std::endl;
      if (p.Float().present()) std::cout << "      Float: " << p.Float()->value() << std::endl;
      if (p.String().present()) std::cout << "      String: " << p.String()->value() << std::endl;
      if (p.Bool().present()) std::cout << "      Bool: " << p.Bool()->value() << std::endl;
      
      auto outputLink = [](const Link &l, std::string_view indent)
      {
        std::cout << indent << "Link:" << std::endl;
        if (l.value().present()) std::cout << indent << "  Value: " << l.value() << std::endl;
        if (l.obj().present()) std::cout << indent << "  Object: " << l.obj() << std::endl;
        if (l.sub().present()) std::cout << indent << "  Sub: " << l.sub() << std::endl;
      };
      if (p.Link().present()) outputLink(p.Link().get(), "      ");
      if (p.LinkList().present())
      {
        std::cout << "      LinkList:" << std::endl;
        std::cout << "        count: " << p.LinkList()->count() << std::endl;
        for (const auto &l : p.LinkList()->Link())
          outputLink(l, "        ");
      }

      if (p.LinkSub().present())
      {
        std::cout << "      LinkSub:" << std::endl;
        std::cout << "        value: " << p.LinkSub()->value() << std::endl;
        std::cout << "        count: " << p.LinkSub()->count() << std::endl;
        for (const auto &ls : p.LinkSub()->Sub())
          std::cout << "        sub: " << ls.value() << std::endl;
      }
      if (p.LinkSubList().present())
      {
        std::cout << "      LinkSubList:" << std::endl;
        std::cout << "        count: " << p.LinkSubList()->count() << std::endl;
        for (const auto &l : p.LinkSubList()->Link())
          outputLink(l, "        ");
      }
      
      if (p.Part().present()) std::cout << "      file: " << p.Part()->file() << std::endl;
    }
    
    void visitObjectDepend(const ObjectDeps &od)
    {
      std::cout << "object base name: " << od.Name() << std::endl;
      for (const auto &dep : od.Dep())
        std::cout << "    Dependency: " << dep.Name() << std::endl;
    }
  };
  
  struct DependentVisitor
  {
    bool skip = false;
    std::vector<std::string> objects;
    std::vector<std::string> referenced;
    std::vector<std::string> shapeObjects;
    const std::set<std::string> skipTypes
    {
      ("PartDesign::Body")
    };
    std::set<std::string> skipNames;
    std::map<std::string, std::string> objectShapeMap;
    std::string currentObjectName;
    
    void visitObject(const Object &o)
    {
      objects.emplace_back(o.name());
      if (o.type().present()) //pretty sure type has to be here in this 'Object' context
      {
        if (skipTypes.count(o.type().get()) != 0)
          skipNames.emplace(o.name());
      }
    }
    
    void visitObjectData(const Object &o)
    {
      if (skipNames.count(o.name()) != 0) skip = true;
      else skip = false;
      currentObjectName = o.name();
    }
    
    void visitObjectDataProperty(const Property &p)
    {
      if (skip) return;
      
      auto add = [&](std::string_view s)
      {
        referenced.emplace_back(s);
        if (referenced.back().empty()) referenced.pop_back(); //we have empty values in files.
      };
      
      if (p.Link().present() && p.Link()->value().present())
        add(p.Link()->value().get());

      if (p.LinkList().present())
      {
        for (const auto &l : p.LinkList()->Link())
        {
          if (l.value().present())
            add(l.value().get());
        }
      }
      
      if (p.LinkSub().present()) add(p.LinkSub()->value());
      
      if (p.LinkSubList().present())
      {
        for (const auto &l : p.LinkSubList()->Link())
          if (l.obj().present())
            add(l.obj().get());
      }
      
      if (p.Part().present())
      {
        shapeObjects.emplace_back(currentObjectName);
        objectShapeMap.insert(std::make_pair(currentObjectName, p.Part()->file()));
      }
    }
    
    void clean()
    {
      auto uniquefy = [](std::vector<std::string> &strings)
      {
        std::sort(strings.begin(), strings.end());
        auto it = std::unique(strings.begin(), strings.end());
        strings.erase(it, strings.end());
      };
      
      uniquefy(objects);
      uniquefy(referenced);
      uniquefy(shapeObjects);
    }
    
    std::vector<std::string> leaves()
    {
      std::vector<std::string> minusSkipped;
      std::set_difference
      (
        objects.begin(), objects.end(),
        skipNames.begin(), skipNames.end()
        , std::back_inserter(minusSkipped)
      );
      
      std::vector<std::string> leavesOut;
      std::set_difference
      (
        minusSkipped.begin(), minusSkipped.end(),
        referenced.begin(), referenced.end()
        , std::back_inserter(leavesOut)
      );
      return leavesOut;
    }
    
    std::vector<std::string> shapeLeaves()
    {
      std::vector<std::string> leafs = leaves();
      std::vector<std::string> leavesOut;
      std::set_intersection
      (
        leafs.begin(), leafs.end(),
        shapeObjects.begin(), shapeObjects.end()
       , std::back_inserter(leavesOut)
      );
      return leavesOut;
    }
  };
}

TEST_CASE("fcexplore0", "[.fcexplore0]")
{
  std::filesystem::path p("../../freecadDocuments/Cover_SHALM/Document.xml");
//   std::filesystem::path p("../../freecadDocuments/PSP_SHALM/Document.xml");
//   std::filesystem::path p("../../freecadDocuments/puma/PUMALite/Document.xml");
//   std::filesystem::path p("../../freecadDocuments/puma/Focus_Gears/Document.xml");
  
  REQUIRE(std::filesystem::exists(p));
  fcx::DExplorer exp(p);
  REQUIRE(exp.isValid());
  
  TestVisitor visitor;
  std::cout << std::endl << "objects: " << std::endl;
  exp.visitObjects<decltype(visitor)>(visitor);
  std::cout << std::endl << "object datas: " << std::endl;
  exp.visitObjectDatas<decltype(visitor)>(visitor);
//   std::cout << std::endl << "object depends: " << std::endl;
//   exp.visitObjectDepends<decltype(visitor)>(visitor);
}

TEST_CASE("fcexplore1", "[.fcexplore1]")
{
  std::filesystem::path p("../../freecadDocuments/Cover_SHALM/Document.xml");
  REQUIRE(std::filesystem::exists(p));
  fcx::DExplorer exp(p);
  REQUIRE(exp.isValid());
  
  DependentVisitor visitor;
  exp.visitObjects<decltype(visitor)>(visitor);
  exp.visitObjectDatas<decltype(visitor)>(visitor);
  visitor.clean();
  
  std::cout << std::endl << "objects: " << std::endl;
  for (const auto &s : visitor.objects)
    std::cout << s << std::endl;
  
  std::cout << std::endl << "referenced: " << std::endl;
  for (const auto &s : visitor.referenced)
    std::cout << s << std::endl;
  
  auto leaves = visitor.leaves();
  std::cout << std::endl << "leaves: " << std::endl;
  for (const auto &s : leaves)
    std::cout << s << std::endl;
  
  auto shapeLeaves = visitor.shapeLeaves();
  std::cout << std::endl << "shape leaves: " << std::endl;
  for (const auto &s : shapeLeaves)
    std::cout << s << std::endl;
}

TEST_CASE("lala", "[lala]")
{
  std::filesystem::path p("../../freecadDocuments/lala/Document.xml");
  REQUIRE(std::filesystem::exists(p));
  fcx::DExplorer exp(p);
  REQUIRE(exp.isValid());
  const auto &fcdoc = exp.getDocument();
  
  //test properties
  REQUIRE(fcdoc.Properties().Count() == 15);
  CHECK(fcdoc.Properties().Count() == static_cast<int>(fcdoc.Properties().Property().size()));
  const auto &testProperty = fcdoc.Properties().Property().at(1);
  CHECK(testProperty.name() == "Company");
  CHECK(testProperty.type() == "App::PropertyString");
  REQUIRE(testProperty.status().present());
  CHECK(testProperty.status().get() == 1);
  REQUIRE(testProperty.String().present());
  CHECK(testProperty.String()->value() == "Home");
  
  //test transient properties
  REQUIRE(fcdoc.Properties().TransientCount().present());
  CHECK(fcdoc.Properties().TransientCount().get() == static_cast<int>(fcdoc.Properties()._Property().size()));
  const auto &testTProperty = fcdoc.Properties()._Property().at(0);
  CHECK(testTProperty.name() == "FileName");
  CHECK(testTProperty.type() == "App::PropertyString");
  REQUIRE(testTProperty.status().present());
  CHECK(testTProperty.status().get() == 50331649);
  
  //test objects
  REQUIRE(fcdoc.Objects().Count() == 6);
  CHECK(fcdoc.Objects().Count() == static_cast<int>(fcdoc.Objects().Object().size()));
  const auto &testObject = fcdoc.Objects().Object().front();
  REQUIRE(testObject.type().present());
  CHECK(testObject.type().get() == "Part::Feature");
  CHECK(testObject.name() == "Pocket001");
  REQUIRE(testObject.id().present());
  CHECK(testObject.id().get() == 2090);
  
  //test object dependencies
  REQUIRE(fcdoc.Objects().Dependencies().present());
  REQUIRE(fcdoc.Objects().Dependencies().get() == 1);
  const auto &testDep = fcdoc.Objects().ObjectDeps().front();
  CHECK(testDep.Name() == "Pocket001");
  CHECK(testDep.Count() == 0);
  
  //test object data
  REQUIRE(fcdoc.ObjectData().Count() == 6);
  REQUIRE(fcdoc.ObjectData().Count() == static_cast<int>(fcdoc.ObjectData().Object().size()));
  const auto &testObjData = fcdoc.ObjectData().Object().front();
  CHECK(testObjData.name() == "Pocket001");
  REQUIRE(testObjData.Properties().present());
  CHECK(testObjData.Properties().get().Count() == 6);
  REQUIRE(testObjData.Properties().get().TransientCount().present());
  CHECK(testObjData.Properties().get().TransientCount().get() == 0);
  const auto &testObjDataProp = testObjData.Properties().get().Property().at(4);
  CHECK(testObjDataProp.name() == "Shape");
  CHECK(testObjDataProp.type() == "Part::PropertyPartShape");
  REQUIRE(testObjDataProp.Part().present());
  CHECK(testObjDataProp.Part()->file() == "PartShape.brp");
}

TEST_CASE("Cover_SHALM", "[Cover_SHALM]")
{
  std::filesystem::path p("../../freecadDocuments/Cover_SHALM/Document.xml");
  REQUIRE(std::filesystem::exists(p));
  fcx::DExplorer exp(p);
  REQUIRE(exp.isValid());
  const auto &fcdoc = exp.getDocument();
  
  REQUIRE(fcdoc.Objects().Count() == 16);
  CHECK(fcdoc.Objects().Count() == static_cast<int>(fcdoc.Objects().Object().size()));
  CHECK(fcdoc.Objects().Count() == static_cast<int>(fcdoc.Objects().ObjectDeps().size()));
  REQUIRE(fcdoc.Objects().Dependencies().present());
  CHECK(fcdoc.Objects().Dependencies().get() == 1); //value is always 1. Why???
  
  const auto &testDep = fcdoc.Objects().ObjectDeps().front();
  CHECK(testDep.Name() == "Body");
  CHECK(testDep.Count() == 10);
  CHECK(testDep.Dep().at(5).Name() == "Sketch001");
  REQUIRE(fcdoc.ObjectData().Count() == 16);
  REQUIRE(fcdoc.ObjectData().Object().front().Extensions().present());
  REQUIRE(fcdoc.ObjectData().Object().front().Extensions()->Count() == 1);
  REQUIRE(fcdoc.ObjectData().Object().front().Extensions()->Extension().size() == 1);
  const auto &extension = fcdoc.ObjectData().Object().front().Extensions()->Extension().front();
  CHECK(extension.type() == "App::OriginGroupExtension");
  CHECK(extension.name() == "OriginGroupExtension");
  
  //note Object type has both an attribute and an element with the name 'Extensions'.
  //xsdcxx puts a '1' suffix on the attribute. So:
  //'Extensions' is the element
  //'Extensions1' is the attribute
  //test sketch
  const auto &sketch = fcdoc.ObjectData().Object().at(8);
  CHECK(sketch.name() == "Sketch");
  CHECK(sketch.Extensions().present());
  CHECK(sketch.Extensions1().get() == "True");
  REQUIRE(sketch.Properties().present());
  REQUIRE(sketch.Properties()->Count() == 16);
  REQUIRE(sketch.Properties()->Property().size() == 16);
  
  const auto &testProp = sketch.Properties()->Property().at(6);
  CHECK(testProp.name() == "Geometry");
  CHECK(testProp.type() == "Part::PropertyGeometryList");
  REQUIRE(testProp.status().present());
  CHECK(testProp.status().get() == 8192);
  REQUIRE(testProp.GeometryList().present());
  REQUIRE(testProp.GeometryList()->count() == 6);
  
  const auto &lineSegment = testProp.GeometryList()->Geometry().front();
  CHECK(lineSegment.type() == "Part::GeomLineSegment");
  REQUIRE(lineSegment.GeoExtensions().present());
  REQUIRE(lineSegment.GeoExtensions()->count() == 1);
  const auto &geoExt = lineSegment.GeoExtensions()->GeoExtension().front();
  CHECK(geoExt.type() == "Sketcher::SketchGeometryExtension");
  REQUIRE(geoExt.internalGeometryType().present());
  CHECK(geoExt.internalGeometryType().get() == 0);
  REQUIRE(geoExt.geometryModeFlags().present());
  CHECK(geoExt.geometryModeFlags().get() == "00000000000000000000000000000000");
  REQUIRE(geoExt.geometryLayer().present());
  CHECK(geoExt.geometryLayer().get() == 0);
  REQUIRE(lineSegment.LineSegment().present());
  CHECK(fuzzyEqual(lineSegment.LineSegment()->StartX(), 0.0));
  CHECK(fuzzyEqual(lineSegment.LineSegment()->StartY(), 0.0));
  CHECK(fuzzyEqual(lineSegment.LineSegment()->StartZ(), 0.0));
  CHECK(fuzzyEqual(lineSegment.LineSegment()->EndX(), 29.5803329500628536));
  CHECK(fuzzyEqual(lineSegment.LineSegment()->EndY(), 0.0));
  CHECK(fuzzyEqual(lineSegment.LineSegment()->EndZ(), 0.0));
  
  const auto &arc = testProp.GeometryList()->Geometry().at(1);
  CHECK(arc.type() == "Part::GeomArcOfCircle");
  REQUIRE(arc.ArcOfCircle().present());
  CHECK(fuzzyEqual(arc.ArcOfCircle()->CenterX(), 29.5803329500628607));
  CHECK(fuzzyEqual(arc.ArcOfCircle()->CenterY(), 29.8574623215134416));
  CHECK(fuzzyEqual(arc.ArcOfCircle()->CenterZ(), 0.0));
  CHECK(fuzzyEqual(arc.ArcOfCircle()->NormalX(), 0.0));
  CHECK(fuzzyEqual(arc.ArcOfCircle()->NormalY(), 0.0));
  CHECK(fuzzyEqual(arc.ArcOfCircle()->NormalZ(), 1.0));
  if (arc.ArcOfCircle()->AngleXU().present())
    CHECK(fuzzyEqual(arc.ArcOfCircle()->AngleXU().get(), 0.0));
  CHECK(fuzzyEqual(arc.ArcOfCircle()->Radius(), 29.8574623215134416));
  CHECK(fuzzyEqual(arc.ArcOfCircle()->StartAngle(), 4.7123889803846897));
  CHECK(fuzzyEqual(arc.ArcOfCircle()->EndAngle(), 5.8233227473975049));
}

TEST_CASE("batch", "[batch]")
{
  std::filesystem::path batchPath("../../batch.txt");
  REQUIRE(std::filesystem::exists(batchPath));
  REQUIRE(std::filesystem::is_regular_file(batchPath));
  std::ifstream batchFile(batchPath.string());
  REQUIRE(batchFile.is_open());
  
  std::string buffer;
  while (std::getline(batchFile, buffer))
  {
    INFO("batch fail with: " << buffer);
    std::filesystem::path currentPath(buffer);
    bool isThere = std::filesystem::exists(currentPath);
    CHECK(isThere); if (!isThere) continue;
    bool isRegularFile = std::filesystem::is_regular_file(currentPath);
    CHECK(isRegularFile); if (!isRegularFile) continue;
    fcx::DExplorer exp(currentPath);
    CHECK(exp.isValid());
  }
}

TEST_CASE("userBatch", "[.userBatch]")
{
  std::filesystem::path batchPath("../../userBatch.txt");
  REQUIRE(std::filesystem::exists(batchPath));
  REQUIRE(std::filesystem::is_regular_file(batchPath));
  std::ifstream batchFile(batchPath.string());
  REQUIRE(batchFile.is_open());
  
  int begin = -1;
  int end = 10000;
  int current = -1;
  REQUIRE(begin < end);
  std::string buffer;
  while (std::getline(batchFile, buffer))
  {
    current++;
    if (current < begin) continue;
    if (current > end) break;
    
    INFO("line is: " << buffer);
    std::filesystem::path currentPath(buffer);
    bool isThere = std::filesystem::exists(currentPath);
    CHECK(isThere); if (!isThere) continue;
    bool isRegularFile = std::filesystem::is_regular_file(currentPath);
    CHECK(isRegularFile); if (!isRegularFile) continue;
    fcx::DExplorer exp(currentPath);
    CHECK(exp.isValid());
  }
}
