/* libfcexplore. Parsing and exploring FreeCAD files.
 * Copyright (C) 2022  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FCX_FCEXPLORER_H
#define FCX_FCEXPLORER_H

#include <memory>
#include <filesystem>

#include "fcdocument.h"

/*
run this every time schema is altered.
First one generates parser via xsdcxx.
Second one updates the embedded schema file.

xsdcxx cxx-tree --std c++11 --generate-serialization --type-naming ucc \
--hxx-suffix .h --cxx-suffix .cpp --guard-prefix FCD ./fcdocument.xsd \
&& xxd -i fcdocument.xsd > fcdocument_schema.h

*/

namespace fcx
{
  
  /*! @class DExplorer
   * @brief Explore freecad document.xml
   * @details Explore freecad Document.xml.
   * Not GuiDocument.xml. Should I just expose type of
   * xml generated, or keep those types private and expose
   * just strings???? Going with full types.
   */
  class DExplorer
  {
  public:
    DExplorer() = delete;
    DExplorer(const std::filesystem::path&);
    DExplorer(const DExplorer&) = delete;
    DExplorer& operator=(const DExplorer&) = delete;
    DExplorer(DExplorer&&) noexcept = default;
    DExplorer& operator=(DExplorer&&) noexcept = default;
    
    bool isValid(){return static_cast<bool>(document);} //call after construction to ensure successful parse.
    const std::string& getParseMessage(){return parseMessage;} //message from failed parsing and isValid is false.
    
    const Document& getDocument(){return *document;}
    
    template<typename T> void visitObjects(T &t) const
    {
      // static_assert(requires(T t, Object o){t.visitObject(o);});
      for (const auto &o : document->Objects().Object())
      {
        t.visitObject(o);
      }
    }
    
    template<typename T> void visitObjectDatas(T &t) const
    {
      // static_assert(requires(T t, Object o){t.visitObjectData(o);});
      for (const auto &o : document->ObjectData().Object())
      {
        t.visitObjectData(o);
        // if constexpr(requires(T t, Property p){t.visitObjectDataProperty(p);})
        // {
          if (o.Properties())
            for (const auto &p : o.Properties()->Property())
              t.visitObjectDataProperty(p);
        // }
      }
    }
    
    //ObjectDeps might not be very useful. actual links in objectdata properties
    template<typename T> void visitObjectDepends(T &t) const
    {
      // static_assert(requires(T t, ObjectDeps od){t.visitObjectDepend(od);});
      for (const auto &od : document->Objects().ObjectDeps())
      {
        t.visitObjectDepend(od);
      }
    }
  private:
    std::unique_ptr<Document> document;
    std::string parseMessage;
  };
}

#endif //FCX_FCEXPLORER_H
