/* libfcexplore. Parsing and exploring FreeCAD files.
 * Copyright (C) 2022  Thomas S. Anderson blobfish.at.gmx.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>

#include "fcdocument_schema.h"
#include "fcexplorer.h"

using namespace fcx;

namespace
{
  //! write embedded schema file to temp directory and create schema props.
  xml_schema::Properties getSchemaProps()
  {
    std::filesystem::path xsdPath = std::filesystem::temp_directory_path() / "fcdocument.xsd";
    std::string_view embedded(reinterpret_cast<char*>(fcdocument_xsd), fcdocument_xsd_len);
    std::ofstream xsdout(xsdPath.string(), std::ios::out | std::ios::trunc);
    xsdout.write(embedded.data(), embedded.size());
    xml_schema::Properties temp;
    temp.no_namespace_schema_location(xsdPath.string());
    return temp;
  }
}

DExplorer::DExplorer(const std::filesystem::path &pathIn)
{
  try
  {
    document = Document_(pathIn.string(), 0, getSchemaProps());
  }
  catch (const xsd::cxx::xml::invalid_utf16_string&)
  {
    parseMessage = "invalid UTF-16 text in DOM model";
  }
  catch (const xsd::cxx::xml::invalid_utf8_string&)
  {
    parseMessage = "invalid UTF-8 text in object model";
  }
  catch (const xml_schema::Exception& e)
  {
    parseMessage = e.what();
  }
}
